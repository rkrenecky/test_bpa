//
//  NetworkingService.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

final class NetworkingService: DataProvider {
    func getData(from url: URL, completion: @escaping (Result<Data>) -> Void) {
        URLSession(configuration: .default).dataTask(with: url) { data, _, error in
            DispatchQueue.main.async {
                if error != nil {
                    completion(.failure(message: nil))
                    return
                }

                guard let data = data else {
                    completion(.failure(message: nil))
                    return
                }

                completion(.success(data))
            }
        }.resume()
    }
}
