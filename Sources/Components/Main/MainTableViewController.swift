//
//  MainTableViewController.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import UIKit

final class MainTableViewController: UITableViewController {
    private weak var activityIndicator: UIActivityIndicatorView?

    private let viewModel: MainViewModel
    private var sections: [MainViewModel.Section] = [] { didSet { tableView.reloadData() } }

    private var filteredEvenIdSections: [MainViewModel.Section] {
        return sections.map { section in
            MainViewModel.Section(model: section.model, items: section.items.filter { $0.id % 2 != 0 })
        }
    }

    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        super.init(style: .plain)

        addActivityIndicator()
        title = NSLocalizedString("MainTitle", comment: "")
        tableView.register(TabTableViewCell.self, forCellReuseIdentifier: TabTableViewCell.identifier)
    }

    @available (*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        Styles.tableView(tableView: tableView)
        Styles.activityIndicator(activityIndicator: activityIndicator)

        getSections()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        tableView.reloadData()
    }

    private func getSections() {
        activityIndicator?.startAnimating()

        viewModel.getSections { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.activityIndicator?.stopAnimating()

            switch result {
            case .failure(let errorMessage):
                strongSelf.showAlert(with: errorMessage)
            case .success(let sections):
                strongSelf.sections = sections
            }
        }
    }

    private func addActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView()
        self.activityIndicator = activityIndicator
        tableView.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: activityIndicator,
                               attribute: .centerX,
                               relatedBy: .equal,
                               toItem: tableView,
                               attribute: .centerX,
                               multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: activityIndicator,
                               attribute: .centerY,
                               relatedBy: .equal,
                               toItem: tableView,
                               attribute: .centerY,
                               multiplier: 1,
                               constant: 0),
        ])
    }

    private func showAlert(with message: String?) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Retry", comment: ""), style: .default, handler: { _ in
            self.getSections()
        }))

        present(alert, animated: true, completion: nil)
    }

    // MARK:- UITableViewDataSource methods

    override func numberOfSections(in tableView: UITableView) -> Int {
        let sections = UIDevice.current.orientation.isLandscape ? filteredEvenIdSections : self.sections
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sections = UIDevice.current.orientation.isLandscape ? filteredEvenIdSections : self.sections
        return sections[section].items.count
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sections = UIDevice.current.orientation.isLandscape ? filteredEvenIdSections : self.sections
        return sections[section].model
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TabTableViewCell.identifier) as? TabTableViewCell else {
            fatalError("Dequeued reusable cell is not TabTableViewCell type!")
        }

        let sections = UIDevice.current.orientation.isLandscape ? filteredEvenIdSections : self.sections
        cell.tab = sections[indexPath.section].items[indexPath.row]

        return cell
    }
}

extension MainTableViewController {
    struct Styles {
        static func tableView(tableView: UITableView?) {
            tableView?.tableFooterView = UIView()
            tableView?.rowHeight = TabTableViewCell.rowHeight
            tableView?.allowsSelection = false
        }

        static func activityIndicator(activityIndicator: UIActivityIndicatorView?) {
            activityIndicator?.style = .gray
            activityIndicator?.hidesWhenStopped = true
        }
    }
}
