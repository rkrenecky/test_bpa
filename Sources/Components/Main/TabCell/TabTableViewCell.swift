//
//  TabTableViewCell.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import UIKit

final class TabTableViewCell: UITableViewCell {
    static let identifier = "TabTableViewCell"
    static let rowHeight: CGFloat = 90

    private weak var mainStackView: UIStackView?
    private weak var tabIdLabel: UILabel?
    private weak var tabNameLabel: UILabel?
    private weak var lastModifiedLabel: UILabel?

    var tab: Tab? { didSet { updateUI() } }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setupView()
    }

    @available (*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        clearCell()
    }

    private func updateUI() {
        guard let tab = tab else {
            clearCell()
            return
        }

        tabIdLabel?.text = "\"TabId\": \(tab.id)"
        tabNameLabel?.text = "\"TabName\": \"\(tab.name)\""
        lastModifiedLabel?.text = "\"LastModified\": \"\(tab.lastModified)\""
    }

    private func clearCell() {
        tabIdLabel?.text = nil
        tabNameLabel?.text = nil
        lastModifiedLabel?.text = nil
    }

    private func setupView() {
        let tabIdLabel = UILabel()
        self.tabIdLabel = tabIdLabel

        let tabNameLabel = UILabel()
        self.tabNameLabel = tabNameLabel

        let lastModifiedLabel = UILabel()
        self.lastModifiedLabel = lastModifiedLabel

        let mainStackView = UIStackView()
        self.mainStackView = mainStackView
        addSubview(mainStackView)

        mainStackView.axis = .vertical
        mainStackView.distribution = .equalSpacing
        mainStackView.alignment = .leading

        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: mainStackView,
                               attribute: .top,
                               relatedBy: .equal,
                               toItem: safeAreaLayoutGuide,
                               attribute: .top,
                               multiplier: 1,
                               constant: 12),
            NSLayoutConstraint(item: mainStackView,
                               attribute: .leading,
                               relatedBy: .equal,
                               toItem: safeAreaLayoutGuide,
                               attribute: .leading,
                               multiplier: 1,
                               constant: 8),
            NSLayoutConstraint(item: mainStackView,
                               attribute: .trailing,
                               relatedBy: .equal,
                               toItem: safeAreaLayoutGuide,
                               attribute: .trailing,
                               multiplier: 1,
                               constant: 8),
            NSLayoutConstraint(item: mainStackView,
                               attribute: .bottom,
                               relatedBy: .equal,
                               toItem: safeAreaLayoutGuide,
                               attribute: .bottom,
                               multiplier: 1,
                               constant: -12)
            ])

        [tabIdLabel, tabNameLabel, lastModifiedLabel].forEach(mainStackView.addArrangedSubview)
    }
}
