//
//  MainViewModel.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

final class MainViewModel {
    typealias Section = SectionModel<String, Tab>

    private let dataProvider: DataProvider

    init(dataProvider: DataProvider) {
        self.dataProvider = dataProvider
    }

    func getSections(completion: @escaping (Result<[Section]>) -> Void) {
        dataProvider.getData(from: Constants.API.baseURL) { result in
            switch result {
            case .failure:
                completion(.failure(message: NSLocalizedString("Error", comment: "")))
            case .success(let data):
                let decoder = JSONDecoder()

                do {
                    let watchlist = try decoder.decode(Watchlist.self, from: data)
                    let sections = watchlist.watchlistTabs.map {
                        Section(model: $0.title, items: $0.tabs)
                    }

                    completion(.success(sections))
                } catch {
                    completion(.failure(message: NSLocalizedString("Error", comment: "")))
                }
            }
        }
    }
}
