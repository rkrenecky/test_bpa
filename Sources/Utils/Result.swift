//
//  Result.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

enum Result<T: Equatable>: Equatable {
    case success(T)
    case failure(message: String?)
}
