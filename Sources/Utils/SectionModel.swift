//
//  SectionModel.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 20/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

struct SectionModel<Section: Equatable, Item: Equatable>: Equatable {
    var model: Section
    var items: [Item]

    init(model: Section, items: [Item]) {
        self.model = model
        self.items = items
    }
}
