//
//  DataProvider.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 20/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

protocol DataProvider {
    func getData(from url: URL, completion: @escaping (Result<Data>) -> Void)
}
