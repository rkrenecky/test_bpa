//
//  Watchlist.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

struct Watchlist: Codable {
    let watchlistTabs: [WatchlistTab]

    enum CodingKeys: String, CodingKey {
        case watchlistTabs = "WatchlistTabs"
    }
}
