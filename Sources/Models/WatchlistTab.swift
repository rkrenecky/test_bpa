//
//  WatchlistTab.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

struct WatchlistTab: Codable {
    let id: Int
    let title: String
    let tabs: [Tab]

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case title = "Title"
        case tabs = "Tabs"
    }
}
