//
//  Tab.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

struct Tab: Codable, Equatable {
    let id: Int
    let name: String
    let lastModified: String

    enum CodingKeys: String, CodingKey {
        case id = "TabId"
        case name = "TabName"
        case lastModified = "LastModified"
    }
}
