//
//  Constants.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

struct Constants {
    struct API {
        static let baseURL = URL(string: "https://firebasestorage.googleapis.com/v0/b/testtask01-c5746.appspot.com/o/TabList.json?alt=media&token=e99cf91c-d5ce-40b1-8632-7089c4ced96a")!
    }
}
