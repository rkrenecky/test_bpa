//
//  ApplicationWireframe.swift
//  Test_BPA
//
//  Created by Robin Krenecky on 19/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import UIKit

final class ApplicationWireframe {
    private let module: ApplicationModule

    init(module: ApplicationModule) {
        self.module = module
    }

    func entrypoint() -> UIViewController {
        let viewModel = MainViewModel(dataProvider: module.networkingService)
        return UINavigationController(rootViewController: MainTableViewController(viewModel: viewModel))
    }
}
