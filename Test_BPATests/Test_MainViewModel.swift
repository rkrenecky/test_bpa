//
//  Test_MainViewModel.swift
//  Test_BPATests
//
//  Created by Robin Krenecky on 20/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import XCTest
@testable import Test_BPA

class Test_MainViewModel: XCTestCase {
    var expectedValidResult: Result<[MainViewModel.Section]> {
        let internalTabs = [
            Tab(id: 10009, name: "Group Financials", lastModified: "2018-01-22T23:00:00.0000000Z")
        ]
        let externalTabs = [
            Tab(id: 10005, name: "Weekly Share Tracker", lastModified: "2018-05-25T08:30:12.1970000Z"),
            Tab(id: 10006, name: "Monthly Share Tracker", lastModified: "2017-11-06T12:35:18.3200000Z")
        ]

        return Result.success([
            MainViewModel.Section(model: "Internal", items: internalTabs),
            MainViewModel.Section(model: "External", items: externalTabs),
        ])
    }

    var expectedFailureResult: Result<[MainViewModel.Section]> {
        return Result.failure(message: NSLocalizedString("Error", comment: ""))
    }

    func testSuccess() {
        struct ValidDataProvider: DataProvider {
            func getData(from url: URL, completion: @escaping (Result<Data>) -> Void) {
                let path = Bundle.main.path(forResource: "TabList", ofType: "json")!
                let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                completion(.success(data))
            }
        }

        let viewModel = MainViewModel(dataProvider: ValidDataProvider())

        let expectation = self.expectation(description: "")

        viewModel.getSections(completion: { result in
            expectation.fulfill()

            XCTAssertEqual(result, self.expectedValidResult)
        })

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testFailure() {
        struct FailureDataProvider: DataProvider {
            func getData(from url: URL, completion: @escaping (Result<Data>) -> Void) {
                completion(.failure(message: nil))
            }
        }

        let viewModel = MainViewModel(dataProvider: FailureDataProvider())

        let expectation = self.expectation(description: "")

        viewModel.getSections(completion: { result in
            expectation.fulfill()

            XCTAssertEqual(result, self.expectedFailureResult)
        })

        waitForExpectations(timeout: 5, handler: nil)
    }
}
